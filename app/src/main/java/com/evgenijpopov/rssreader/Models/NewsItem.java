package com.evgenijpopov.rssreader.Models;


import android.graphics.Bitmap;

import java.util.Date;

public class NewsItem {
    private String link = "";
    private String title = "";
    private String description = "";
    private Date pubDate;
    private String enclosureUrl = "";
    private String category = "";

    private Bitmap image = null;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubDate() {
        return pubDate == null ? "" : pubDate.toLocaleString();
    }

    public void setPubDate(String pubDate) {
        this.pubDate = new Date(pubDate);
    }

    public String getEnclosureUrl() {
        return enclosureUrl;
    }

    public void setEnclosureUrl(String enclosureUrl) {
        this.enclosureUrl = enclosureUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

}
