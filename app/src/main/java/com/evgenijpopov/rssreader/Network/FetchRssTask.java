package com.evgenijpopov.rssreader.Network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;


import com.evgenijpopov.rssreader.Models.NewsItem;
import com.evgenijpopov.rssreader.NewsViewAdapter;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FetchRssTask extends AsyncTask<NewsViewAdapter, Void, List<NewsItem>> {
    private NewsViewAdapter newsViewAdapter;
    private List<NewsItem> newsItemList;
    int MAX_LIST_SIZE = 30;

    @Override
    protected List<NewsItem> doInBackground(NewsViewAdapter... params) {
        if (params.length == 0) return null;
        String result;
        newsViewAdapter = params[0];

        URL requestUrl = NetworkUtils.buildUrl();
        try {
            result = NetworkUtils.getResponseFromHttpUrl(requestUrl);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


        newsItemList = new ArrayList<>();
        try {
            newsItemList = parseXML(result);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        newsItemList = loadImages(newsItemList);
        return newsItemList;
    }


    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        newsViewAdapter.changeList(newsItemList);
    }

    @Override
    protected void onPostExecute(List<NewsItem> items) {
        newsViewAdapter.changeList(newsItemList);
    }

    private List<NewsItem> parseXML(String inputXML) throws XmlPullParserException, IOException {
        if (inputXML == null || inputXML.equals("")) return null;
        List<NewsItem> newsItemList = new ArrayList<>();

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlParser = factory.newPullParser();
        xmlParser.setInput(new StringReader(inputXML));
        int eventType = xmlParser.getEventType();
        boolean inItem = false;
        String tagName = "", tagText = "";
        NewsItem newsItem = null;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                tagName = xmlParser.getName();
                if (tagName.equals("item")) {
                    inItem = true;
                    newsItem = new NewsItem();
                } else if (tagName.equals("enclosure") && inItem) {
                    newsItem.setEnclosureUrl(xmlParser.getAttributeValue(null, "url"));
                }
            } else if (eventType == XmlPullParser.TEXT && inItem) {
                tagText = xmlParser.getText() == null ? "" : xmlParser.getText().trim();

                switch (tagName) {
                    case "title": {
                        if (!tagText.equals(""))
                            newsItem.setTitle(newsItem.getTitle() + tagText);
                        break;
                    }
                    case "link": {
                        if (!tagText.equals(""))
                            newsItem.setLink(newsItem.getLink() + tagText);
                        break;
                    }
                    case "description": {
                        if (!tagText.equals(""))
                            newsItem.setDescription(newsItem.getDescription() + tagText);
                        break;
                    }
                    case "pubDate": {
                        if (!tagText.equals(""))
                            newsItem.setPubDate(tagText);
                        break;
                    }
                    case "category": {
                        if (!tagText.equals(""))
                            newsItem.setCategory(tagText);
                        break;
                    }
                }

            } else if (eventType == XmlPullParser.END_TAG) {
                if (xmlParser.getName().equals("item")) {
                    inItem = false;
                    newsItemList.add(newsItem);
                }
            }
            if (newsItemList.size() > MAX_LIST_SIZE) break;
            eventType = xmlParser.next();
        }
        return newsItemList;
    }

    private List<NewsItem> loadImages(List<NewsItem> newsItems) {
        for (NewsItem item: newsItems) {
            if (item.getEnclosureUrl().equals("")) continue;
            URL url = null;
            Bitmap image = null;
            try {
                url = new URL(item.getEnclosureUrl());
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                item.setImage(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
            publishProgress();
        }
        return newsItems;
    }
}


