package com.evgenijpopov.rssreader;

import android.content.Context;
import android.content.Intent;

import com.evgenijpopov.rssreader.UI.WebViewActivity;


public class NewsListClickListener implements IClickListener {
    private NewsViewAdapter newsViewAdapter;
    private Context context;

    public void setNewsViewAdapter(NewsViewAdapter newsViewAdapter) {
        this.newsViewAdapter = newsViewAdapter;
    }

    public NewsListClickListener(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(int i) {
        String link = newsViewAdapter.getLink(i);
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra("link", link);
        context.startActivity(intent);
    }
}
