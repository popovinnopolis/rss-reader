package com.evgenijpopov.rssreader;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgenijpopov.rssreader.Models.NewsItem;

import java.util.List;


public class NewsViewAdapter extends RecyclerView.Adapter<NewsViewHolder> {
    private List<NewsItem> newsItemList;
    private IClickListener clickListener;

    public NewsViewAdapter(List<NewsItem> newsItemList, IClickListener clickListener) {
        this.newsItemList = newsItemList;
        this.clickListener = clickListener;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new NewsViewHolder(v, clickListener);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        holder.bind(newsItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return newsItemList.size();
    }

    public void changeList(List<NewsItem> newsItemList) {
        this.newsItemList = newsItemList;
        notifyDataSetChanged();
    }

    public String getLink(int i) {
        return newsItemList.get(i).getLink();
    }
}
