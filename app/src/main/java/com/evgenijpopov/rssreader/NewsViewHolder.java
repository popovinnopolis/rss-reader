package com.evgenijpopov.rssreader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evgenijpopov.rssreader.Models.NewsItem;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class NewsViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView tv_link;
    private TextView tv_title;
    private TextView tv_description;
    private TextView tv_pubDate;
    private TextView tv_enclosureUrl;
    private TextView tv_category;
    private ImageView iv_Image;

    private IClickListener clickListener;

    public NewsViewHolder(View itemView, IClickListener clickListener) {
        super(itemView);
        tv_title = (TextView) itemView.findViewById(R.id.item_title);
        tv_description = (TextView) itemView.findViewById(R.id.item_description);
        tv_pubDate = (TextView) itemView.findViewById(R.id.item_pub_date);
        tv_category = (TextView) itemView.findViewById(R.id.item_category);
        iv_Image = (ImageView) itemView.findViewById(R.id.iv_image);

        this.clickListener = clickListener;
        itemView.setOnClickListener(this);
    }

    public void bind(NewsItem newsItem) {
        tv_title.setText(newsItem.getTitle());
        tv_description.setText(newsItem.getDescription());
        tv_pubDate.setText(newsItem.getPubDate());
        tv_category.setText(newsItem.getCategory());

        iv_Image.setImageBitmap(newsItem.getImage());

    }

    @Override
    public void onClick(View v) {
        if (clickListener != null) clickListener.onClick(getAdapterPosition());
    }
}
