package com.evgenijpopov.rssreader.UI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.evgenijpopov.rssreader.Models.NewsItem;
import com.evgenijpopov.rssreader.Network.FetchRssTask;
import com.evgenijpopov.rssreader.NewsListClickListener;
import com.evgenijpopov.rssreader.NewsViewAdapter;
import com.evgenijpopov.rssreader.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView newsRecyclerView;
    private List<NewsItem> newsItemList;
    private NewsViewAdapter newsViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        newsRecyclerView = (RecyclerView) findViewById(R.id.newsRecyclerView);
        newsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        newsItemList = new ArrayList<>();

        NewsListClickListener newsListClickListener = new NewsListClickListener(this);
        newsViewAdapter = new NewsViewAdapter(newsItemList, newsListClickListener);
        newsListClickListener.setNewsViewAdapter(newsViewAdapter);
        newsRecyclerView.setAdapter(newsViewAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadRSSData();
    }

    private void loadRSSData() {
        new FetchRssTask().execute(newsViewAdapter);
    }

}
